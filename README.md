# Directus installation

1. Clone the repository
2. Edit the file "directus.yml" with the required information
3. Run the command ````vagrant up ```` inside the directory

By default Directus is available at localhost, port 8080.
You will have to edit your hosts file to access it.
On Windows it is in ````C:\Windows\System32\drivers\etc\hosts````, GNU/Linux and MacOS - /etc/hosts.
Add to the file the following line:

````127.0.0.1 www.directus.local````

After the playbook is installed, you'll be able to connect to it by opening your browser and accessing ````www.directus.local:8080````

The domain_name is defined in the directus.yml file